  $('[data-toggle="tooltip"]').tooltip();
  $('[data-toggle="popover"]').popover();
  $(".carousel").carousel({
    interval: 2000,
  });
  //CUANDO SE ABRE EL MODAL DESAYUNO
  $("#desayunoBtn").on("click", function () {
    $("#modal-gastronomia").on("show.bs.modal", function (e) {
      $("#desayunoBtn").removeClass("btn btn-info");
      $("#desayunoBtn").addClass("btn btn-defauld");
      $("#desayunoBtn").prop("disabled", true);
      let title_card = $(".title_desayuno").text();
      $(".title_modal").text(`${title_card}`);
      $(".formulario").html(`
        <h6 class="text-center">Café</h6>
        <div class="row">
          <div class="col-sm-4">
            <div class="form-check">
                <input type="checkbox" class="form-check-input">
                <label class="form-check-label" for="exampleCheck1">Azúcar</label>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-check">
                <input type="checkbox" class="form-check-input">
                <label class="form-check-label" for="exampleCheck1">Edulcorante</label>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-check">
                <input type="checkbox" class="form-check-input">
                <label class="form-check-label" for="exampleCheck1">Leche</label>
            </div>
          </div>
        </div>
        <hr>
        <h6 class="text-center">Pan</h6>
        <div class="row">

          <div class="col-sm-4">
            <div class="form-check">
                <input type="checkbox" class="form-check-input">
                <label class="form-check-label" for="exampleCheck1">Integral</label>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-check">
                <input type="checkbox" class="form-check-input">
                <label class="form-check-label" for="exampleCheck1">Blanco</label>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-check">
                <input type="checkbox" class="form-check-input">
                <label class="form-check-label" for="exampleCheck1">Tostado</label>
            </div>
          </div>
        </div>
        `);
    });
  });

  //CUANDO SE CIERRA EL MODAL DE DESAYUNO
  $("#modal-gastronomia").on("hide.bs.modal", function (e) {
    $("#desayunoBtn").removeClass("btn btn-defauld");
    $("#desayunoBtn").addClass("btn btn-info");
    $("#desayunoBtn").prop("disabled", false);
  });
  //CUANDO SE ABRE EL MODAL MEDIA MAÑANA
  $("#media_manhanaBtn").on("click", function () {
    $("#modal-gastronomia").on("show.bs.modal", function (e) {
      $("#media_manhanaBtn").removeClass("btn btn-info");
      $("#media_manhanaBtn").addClass("btn btn-primary");
      $("#media_manhanaBtn").prop("disabled", true);
      let title_card = $(".title_media_manhana").html();
      $(".title_modal").html(`${title_card}`);
      $(".formulario").html(`
        <h6 class="text-center">Jugo</h6>
        <div class="row">
          <div class="col-sm-4">
            <div class="form-check">
                <input type="checkbox" class="form-check-input">
                <label class="form-check-label" for="exampleCheck1">Azúcar</label>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-check">
                <input type="checkbox" class="form-check-input">
                <label class="form-check-label" for="exampleCheck1">Edulcorante</label>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-check">
                <input type="checkbox" class="form-check-input">
                <label class="form-check-label" for="exampleCheck1">Hielo</label>
            </div>
          </div>
        </div>
        <hr>
        <h6 class="text-center">Donas</h6>
        <div class="row">

          <div class="col-sm-4">
            <div class="form-check">
                <input type="checkbox" class="form-check-input">
                <label class="form-check-label" for="exampleCheck1">Vainilla</label>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-check">
                <input type="checkbox" class="form-check-input">
                <label class="form-check-label" for="exampleCheck1">Chocolate</label>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-check">
                <input type="checkbox" class="form-check-input">
                <label class="form-check-label" for="exampleCheck1">Fresas</label>
            </div>
          </div>
        </div>
        `);
    });
  });
  // CUANDO SE CIERRA EL MODAL DE MEDIA MAÑANA
  $("#modal-gastronomia").on("hide.bs.modal", function (e) {
    $("#media_manhanaBtn").removeClass("btn btn-primary");
    $("#media_manhanaBtn").addClass("btn btn-info");
    $("#media_manhanaBtn").prop("disabled", false);
  });
  //CUANDO SE ABRE EL MODAL DE ALMUERZO
  $("#almuerzoBtn").on("click", function () {
    $("#modal-gastronomia").on("show.bs.modal", function (e) {
      $("#almuerzoBtn").removeClass("btn btn-info");
      $("#almuerzoBtn").addClass("btn btn-primary");
      $("#almuerzoBtn").prop("disabled", true);
      let title_card = $(".title_almuerzo").html();
      $(".title_modal").html(`${title_card}`);
      $(".formulario").html(`
        <h6 class="text-center">Milanesa</h6>
        <div class="row">
          <div class="col-sm-4">
            <div class="form-check">
                <input class="form-check-input" type="radio" value="carne">
                <label class="form-check-label" for="exampleCheck1">De carne</label>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-check">
                <input class="form-check-input" type="radio" value="pollo">
                <label class="form-check-label" for="exampleCheck1">De Pollo</label>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-check">
                <input class="form-check-input" type="radio" value="pescado">
                <label class="form-check-label" for="exampleCheck1">De Pescado</label>
            </div>
          </div>
        </div>
        <hr>
        <h6 class="text-center">Complemento</h6>
        <div class="row">

          <div class="col-sm-4">
            <div class="form-check">
                <input type="checkbox" class="form-check-input">
                <label class="form-check-label" for="exampleCheck1">Pure</label>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-check">
                <input type="checkbox" class="form-check-input">
                <label class="form-check-label" for="exampleCheck1">Arroz Blanco</label>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-check">
                <input type="checkbox" class="form-check-input">
                <label class="form-check-label" for="exampleCheck1">Ensalada de lechuga</label>
            </div>
          </div>
        </div>
        `);
    });
  });
  //CUANDO SE CIERRA EL MODAL DE ALMUERZO
  $("#modal-gastronomia").on("hide.bs.modal", function () {
    $("#almuerzoBtn").removeClass("btn btn-primary");
    $("#almuerzoBtn").addClass("btn btn-info");
    $("#almuerzoBtn").prop("disabled", false);
  });
  //CUANDO SE ABRE EL MODAL DE MERIENDA
  $("#meriendaBtn").on("click", function () {
    $("#modal-gastronomia").on("show.bs.modal", function (e) {
      $("#meriendaBtn").removeClass("btn btn-info");
      $("#meriendaBtn").addClass("btn btn-primary");
      $("#meriendaBtn").prop("disabled", true);
      let title_card = $(".title_merienda").html();
      $(".title_modal").html(`${title_card}`);
      $(".formulario").html(`
      <h6 class="text-center">Jugo</h6>
        <div class="row">
          <div class="col-sm-4">
            <div class="form-check">
                <input type="checkbox" class="form-check-input">
                <label class="form-check-label" for="exampleCheck1">Azúcar</label>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-check">
                <input type="checkbox" class="form-check-input">
                <label class="form-check-label" for="exampleCheck1">Edulcorante</label>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-check">
                <input type="checkbox" class="form-check-input">
                <label class="form-check-label" for="exampleCheck1">Hielo</label>
            </div>
          </div>
        </div>
        <hr>
        <h6 class="text-center">Galletas  </h6>
        <div class="row">

          <div class="col-sm-4">
            <div class="form-check">
                <input type="checkbox" class="form-check-input">
                <label class="form-check-label" for="exampleCheck1">Vainilla</label>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-check">
                <input type="checkbox" class="form-check-input">
                <label class="form-check-label" for="exampleCheck1">Chocolate</label>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-check">
                <input type="checkbox" class="form-check-input">
                <label class="form-check-label" for="exampleCheck1">Fresas</label>
            </div>
          </div>
        </div>
        `);
    });
  });
  //CUANDO SE CIERRA EL MODAL
  $("#modal-gastronomia").on("hide.bs.modal", function () {
    $("#meriendaBtn").removeClass("btn btn-primary");
    $("#meriendaBtn").addClass("btn btn-info");
    $("#meriendaBtn").prop("disabled", false);
  });
  //CUANDO SE ABRE EL MODAL CENA
  $("#cenaBtn").on("click", function () {
    $("#modal-gastronomia").on("show.bs.modal", function (e) {
      $("#cenaBtn").removeClass("btn btn-info");
      $("#cenaBtn").addClass("btn btn-primary");
      $("#cenaBtn").prop("disabled", true);
      let title_card = $(".title_cena").html();
      $(".title_modal").html(`${title_card}`);
      $(".formulario").html(`
        <h6 class="text-center">Pollo</h6>
        <div class="row">
          <div class="col-sm-4">
            <div class="form-check">
                <input class="form-check-input" type="radio" value="carne">
                <label class="form-check-label" for="exampleCheck1">Muslo</label>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-check">
                <input class="form-check-input" type="radio" value="pollo">
                <label class="form-check-label" for="exampleCheck1">Pechuga</label>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-check">
                <input class="form-check-input" type="radio" value="pescado">
                <label class="form-check-label" for="exampleCheck1">Alitas</label>
            </div>
          </div>
        </div>
        <hr>
        <h6 class="text-center">Complemento</h6>
        <div class="row">

          <div class="col-sm-4">
            <div class="form-check">
                <input type="checkbox" class="form-check-input">
                <label class="form-check-label" for="exampleCheck1">Pure</label>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-check">
                <input type="checkbox" class="form-check-input">
                <label class="form-check-label" for="exampleCheck1">Arroz Blanco</label>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-check">
                <input type="checkbox" class="form-check-input">
                <label class="form-check-label" for="exampleCheck1">Ensalada de lechuga</label>
            </div>
          </div>
        </div>
        `);
    });
  });
  //CUANDO SE CIERRA EL MODAL CENA
  $("#modal-gastronomia").on("hide.bs.modal", function () {
    $("#cenaBtn").removeClass("btn btn-primary");
    $("#cenaBtn").addClass("btn btn-info");
    $("#cenaBtn").prop("disabled", false);
  });
